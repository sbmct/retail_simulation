"""Generate paths from a list of waypoints

Usage: pathfinder.py [-q]

Options:
  -q      # suppress output 
"""

from docopt import docopt
from scipy import misc
from scipy import ndimage
import numpy
import time
from astar import astar
from PIL import Image
from utilities import get_spiral
import random
import string


def get_item_locations(itemCount,scalar):
    return [[int(957*scalar),int(953*scalar)],
            [int(156*scalar),int(498*scalar)],
            [int(642*scalar),int(1154*scalar)],
            [int(600*scalar),int(1152*scalar)],
            [int(593*scalar),int(1249*scalar)],
            [int(390*scalar),int(1250*scalar)],
            [int(371*scalar),int(1262*scalar)],
            [int(353*scalar),int(1274*scalar)],
            [int(241*scalar),int(1047*scalar)],
            [int(253*scalar),int(1047*scalar)],
            [int(263*scalar),int(1047*scalar)],
            [int(269*scalar),int(1047*scalar)],
            [int(350*scalar),int(1047*scalar)],
            [int(411*scalar),int(1047*scalar)],
            [int(401*scalar),int(1027*scalar)],
            [int(395*scalar),int(1027*scalar)],
            [int(341*scalar),int(1027*scalar)],
            [int(302*scalar),int(1027*scalar)],
            [int(268*scalar),int(1027*scalar)],
            [int(253*scalar),int(1010*scalar)],
            [int(404*scalar),int(1010*scalar)],
            [int(408*scalar),int(991*scalar)],
            [int(399*scalar),int(991*scalar)],
            [int(383*scalar),int(991*scalar)],
            [int(351*scalar),int(973*scalar)],
            [int(327*scalar),int(973*scalar)],
            [int(298*scalar),int(973*scalar)],
            [int(287*scalar),int(973*scalar)],
            [int(263*scalar),int(973*scalar)],
            [int(253*scalar),int(973*scalar)],
            [int(250*scalar),int(954*scalar)],
            [int(340*scalar),int(954*scalar)],
            [int(245*scalar),int(945*scalar)],
            [int(345*scalar),int(917*scalar)],
            [int(276*scalar),int(917*scalar)],
            [int(378*scalar),int(917*scalar)],
            [int(404*scalar),int(900*scalar)]]

def scale_locations(locations, scaleFactor):
    scaledLocations = []
    for location in locations:
        scaledLocations.append([int(location[0] * scaleFactor),int(location[1] * scaleFactor)])
    return scaledLocations

def align_locations(locations, grid):
    negative = numpy.invert(grid)
    nearest_floor = ndimage.distance_transform_edt(negative, return_distances=False, return_indices=True)
    realigned = []
    for loc in locations:
        if not grid[loc[0],loc[1]] == PASSIBLE: 
            realigned.append([nearest_floor[0, loc[0], loc[1]],
                nearest_floor[1, loc[0], loc[1]]])
        else:
            realigned.append(loc)
    return realigned

def find_path(locations, mainGrid, costGrid, coarseGrid, debug=False):
    global DEBUG
    DEBUG = debug
    SCALE_FACTOR = float(mainGrid.shape[0]) / coarseGrid.shape[0] #rounding this can change result significantly, good or bad?
    expandedCoarsePath = numpy.zeros(mainGrid.shape, numpy.bool)

    def expand_coarse_path(coarsePath, expandedCoarsePath):
        RADIUS = int(3 * round(SCALE_FACTOR))
        spiral = get_spiral((0,0), RADIUS)
        for segment in coarsePath:
            for point in segment:
                scaledPoint = (point[0]*SCALE_FACTOR,point[1]*SCALE_FACTOR)
                for sp in spiral:
                    expandedPoint = (sp[0] + scaledPoint[0]), (sp[1] + scaledPoint[1])
                    expandedCoarsePath[expandedPoint] = True
                        
    coarsePath = find_coarse_path(align_locations(scale_locations(locations, (1.0 / SCALE_FACTOR)), coarseGrid), coarseGrid)
    expand_coarse_path(coarsePath, expandedCoarsePath)
    return find_fine_path(align_locations(locations, mainGrid), mainGrid, costGrid, expandedCoarsePath)


def find_coarse_path(locations, coarseGrid):
    pathfinder = CoarsePathfinder(locations, coarseGrid)
    pathfinder.generate_path()
    return pathfinder.astarPath 

def find_fine_path(locations, grid, costGrid, coarsePath):
    pathfinder = FinePathfinder(locations, grid, costGrid, coarsePath)
    pathfinder.generate_path()
    return pathfinder.astarPath 
    

def getOneStepOrthogonalNeighbors(y,x):
    return [[[y+1,x]],
             [[y-1,x]],
             [[y,x+1]],
             [[y,x-1]]]


def getSixStepOrthogonalNeighbors(y,x):
    return [[[y+1,x],[y+2,x],[y+3,x],[y+4,x],[y+5,x],[y+6,x]], #down
            [[y-1,x],[y-2,x],[y-3,x],[y-4,x],[y-5,x],[y-6,x]], #up
            [[y,x+1],[y,x+2],[y,x+3],[y,x+4],[y,x+5],[y,x+6]], #right
            [[y,x-1],[y,x-2],[y,x-3],[y,x-4],[y,x-5],[y,x-6]]]  #left
                
class Pathfinder(object):
    def __init__(self,locations,grid):
        self.pathPoints = locations 
        self.grid = grid
        self.myGoal = []
        self.myNodes = []
        self.astarPath = []
        self.INVALID_POINT = [-1,-1]
        self.previousPoint = self.INVALID_POINT
        self.pathClear = False
        self.NODES = int((self.grid.shape[0] * self.grid.shape[1])**0.75) 

    def getAdjacentCells(self, y, x):
        return getOneStepOrthogonalNeighbors(y,x)

    def goal(self, pos):
        return pos in self.pathPoints

    def neighbors(self, pos):
        y, x = pos
        potentialNeighbors = self.getAdjacentCells(y,x)
        neighbors = []    
        
        for direction in potentialNeighbors:
            unobstructed = True
            for cell in direction:
                if self.grid[cell[0], cell[1]] == 0:
                    unobstructed = False
                    break
            if unobstructed == True:
                neighbors.append((cell[0], cell[1]))
        return neighbors

    def distance(self, dx, dy):
        dx = abs(dx)
        dy = abs(dy)
        return dx + dy
    
    def debug(self, nodes):
        myNodes = nodes

    def generate_path(self):
        for point in self.pathPoints:
            if self.previousPoint == self.INVALID_POINT:
                self.previousPoint = point
                continue
            self.myGoal = point
            myPos = self.previousPoint
            if DEBUG:
                print myPos, self.myGoal,
            astarStart = time.clock()
            self.astarPath.append(astar((myPos[0], myPos[1]), self.neighbors, self.goal, 0, self.cost, self.estimate, self.NODES, self.debug))
            if DEBUG:
                print time.clock() - astarStart, len(self.astarPath[len(self.astarPath)-1])
            self.previousPoint = point
            self.pathClear = False
        if DEBUG:
            self.display_path_as_bitmap()

    def display_path_as_bitmap(self):
        pathMap = numpy.zeros((self.grid.shape[0], self.grid.shape[1], 4), numpy.int) 
        print "generating image"
        for y in range(0, pathMap.shape[0]):
            for x in range(0, pathMap.shape[1]):
                if self.grid[y,x] == PASSIBLE:
                    pathMap[y,x] = [200,200,200,255]

        colors = [[255,0,255,255],[0,255,255,255]]
        colorIdx = 0
        for path in self.astarPath:
            for point in path:
                pathMap[point[0],point[1]] = colors[colorIdx] 
            colorIdx += 1
            if colorIdx >= len(colors):
                colorIdx = 0
            
        filename = "tmp_map_" + ''.join(random.choice(string.letters) for i in xrange(6)) + ".png"
        print "saving image", filename
        misc.imsave(filename, pathMap)
        Image.open(filename).show()


class CoarsePathfinder(Pathfinder):
    def __init__(self,locations,grid):
        super(CoarsePathfinder, self).__init__(locations, grid)

    def cost(self, from_pos, to_pos):
        from_y, from_x = from_pos
        to_y, to_x = to_pos
        return self.distance(from_x - to_x, from_y - to_y)

    def estimate(self, pos, parent):
        y, x = pos
        goal_y, goal_x = self.myGoal
        return self.distance(goal_x - x, goal_y - y)


class FinePathfinder(Pathfinder):
    def __init__(self,locations,grid,costGrid,coarsePath):
        super(FinePathfinder, self).__init__(locations, grid)
        self.costGrid = costGrid
        self.coarsePath = coarsePath
        self.NODES = 5e3 

    def getAdjacentCells(self, y, x):
        return getSixStepOrthogonalNeighbors(y,x)

    def cost(self, from_pos, to_pos):
        from_y, from_x = from_pos
        to_y, to_x = to_pos
    
        dist = self.distance(from_x - to_x, from_y - to_y)
        offPath = 100 if not self.coarsePath[to_y, to_x] else 1
        difficulty = 256 - self.costGrid[to_y,to_x][0]
        return difficulty * dist * offPath

    def estimate(self, pos, parent):
        y, x = pos
        goal_y, goal_x = self.myGoal
        y_p, x_p = parent
        hitObstacle = 1
        offPath = 1
        
        #Everything in this if check may not be useful
        if x_p >= 0:
            dy = (goal_y - y)
            dx = (goal_x - x)
            if dx > 0:
                rangeMax = dx 
                slope = dy / dx
                dy = slope
                dx = 1
            else:
                rangeMax = dy 
                dx = 0
                dy = 1
                
            x_i = x
            y_i = y
            self.pathClear = True
            for i in range(0, rangeMax, 30):
                y_i += dy
                x_i += dx
                if self.grid[y_i, x_i] == OBSTACLE:
                    self.pathClear = False
                    break
            if self.pathClear == False: 
                dy = y - y_p
                dx = x - x_p
                y_i = y
                x_i = x
                y_max = self.grid.shape[0]
                x_max = self.grid.shape[1]
                for i in range(0, 10):
                    x_i += dx
                    y_i += dy
                    if (x_i >= x_max) or (x_i < 0):
                        hitObstacle = 255
                        break
                    elif (y_i >= y_max) or (y_i < 0):
                        hitObstacle = 255
                        break
                    elif self.grid[y_i, x_i] == OBSTACLE:
                        hitObstacle = 255
                        break
                    if self.coarsePath[y_i, x_i] == False:
                        offPath = 100
                        break
                
        dist = self.distance(goal_x - x, goal_y - y)
        
        return dist * hitObstacle * offPath 
    

DEBUG = False
OBSTACLE = 0
PASSIBLE = 255

def main():
    debug = True
    opts = docopt(__doc__)
    if opts.get('-q'):
        debug = False
    costMap = misc.imread('cost_map.png')
    mainMap = misc.imread('map.png')
    coarseMap = misc.imread('coarse_map.png')

    astarPath = find_path(get_item_locations(0,1), mainMap, costMap, coarseMap, debug)

if __name__ == '__main__':
    main()
