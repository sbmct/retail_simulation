"""queue obs data protein generator

Usage: retail_sim.py [-v]

Options:
  -v  # display simulation statistics
"""
from docopt import docopt
from scipy import misc 
import collections
import dateutil
import itertools
import os.path
import pickle
import math
from multiprocessing import Pool, cpu_count
from pathfinder import pathfinder
import random

poi = []
entryPoints = []
exitPoints = []
originPoints = []
queueWaitPoints = []
checkoutPoints = []
WHITE = [255,255,255,255]
RED = [255,0,0,255]
GREEN = [0,255,0,255]
BLUE = [0,0,255,255]
_12_HOURS = 3600 * 12
SIM_DURATION = _12_HOURS
CASHIER_COUNT = 15
lanesOpen = 6
ITEMS_SCANNED_PER_SECOND = ( 600 / float(3600) )
TRANSACTION_TIME_SECONDS = 45
CUSTOMERS_ENTERING_PER_SECOND = 1. / 56
UTC_REFERENCE = 1383319000
#UTC_REFERENCE = 1417150800 #5AM black friday 2014 GMT
#UTC_REFERENCE = 1417179600 #5AM black friday 2014 PST
#averageShoppingDurationSeconds = ( 40 * 60 )

queues = []
shoppers = []
satisfiedCustomers = []
shopperId = 1


def initialize_queues():
    laneStatus = []
    for i in range(0, CASHIER_COUNT):
        laneStatus.append(False)

    for i in range(0, lanesOpen):
        laneNumber = random.randint(0, CASHIER_COUNT - 1)
        while laneStatus[laneNumber] == True:
            laneNumber = random.randint(0, CASHIER_COUNT - 1)
        laneStatus[laneNumber] = True

    for i in range(0, CASHIER_COUNT):
        queues.append(dict(laneNumber=i, laneOpen=laneStatus[i], shoppers=[]))

    testOpenLaneCount = 0
    for queue in queues:
        if queue['laneOpen']:
            testOpenLaneCount += 1

    if testOpenLaneCount != lanesOpen:
        print "ERROR in initialize_queues", lanesOpen, "should be open but only", testOpenLaneCount, "are"

def add_to_queue(shopper, time):
    shopper['enterQueueTime'] = time
    shortestQueueLength = 1e6
    shortQueues = []
    for queue in queues:
        shoppersInQueue = queue['shoppers']
        if len(shoppersInQueue) < shortestQueueLength and queue['laneOpen']:
            shortestQueueLength = len(shoppersInQueue)
            shortQueues = [queue]
        elif len(shoppersInQueue) == shortestQueueLength and queue['laneOpen']:
            shortQueues.append(queue)

    if len(shortQueues) > 1:
        chosenQueue = shortQueues[random.randint(0, len(shortQueues) - 1)]
    else:
        chosenQueue = shortQueues[0]

    shopper['assignedQueue'] = chosenQueue['laneNumber']
    shopper['queueLength'] = len(chosenQueue['shoppers'])
    insert_queue_in_path(shopper)
    if not chosenQueue['shoppers']:
        shopper['startCheckoutTime'] = time

    chosenQueue['shoppers'].append(shopper)


def start_next_checkout(queue, time):
    if queue:
        shopper = queue[0]
        shopper['startCheckoutTime'] = time


def get_random_element(theList):
    if len(theList) == 0:
        raise IndexError
    return theList[random.randint(0,len(theList)-1)]


def get_item_locations(count):
    locations = []
    for i in xrange(0,count):
        locations.append(get_random_element(poi))
    return locations


def get_origin_point():
    return get_random_element(originPoints)


def get_entry_point():
    return get_random_element(entryPoints)


def get_exit_point():
    return get_random_element(exitPoints)


def distance(a,b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)


def reorder_locations(entry, locations):
    reordered = []
    currentLoc = entry
    while (len(locations) > 0):
        shortest = 1e99
        nextLoc = []
        for location in locations:
            thisDistance = distance(currentLoc, location)
            if thisDistance < shortest:
                shortest = thisDistance
                nextLoc = location
        reordered.append(nextLoc)
        locations.remove(nextLoc)
        currentLoc = nextLoc
    return reordered
            
def get_pre_checkout_waypoint(point):
    Y = 620
    X_L = 620
    X_R = 860
    y = Y
    x = X_L if point[1] < X_L else X_R if point[1] > X_R else point[1]
    return [y,x]

def get_post_checkout_waypoint(point):
    y = 750
    x = point[1] + 10
    return [y,x]

def insert_queue_in_path(shopper):
    laneNumber = shopper['assignedQueue']
    preCheckout = get_pre_checkout_waypoint(shopper['waypoints'][-1])
    waitPoint = list(queueWaitPoints[laneNumber])
    #if shopper['queueLength'] >= 2
    #    depth = (shopper['queueLength'] - 1) * 2
    #    waitPoint[0] = waitPoint[0] - depth
    checkoutPoint = checkoutPoints[laneNumber]
    shopper['waypoints'].append(preCheckout)
    shopper['waypoints'].append(waitPoint)
    shopper['waypoints'].append(checkoutPoint)
    shopper['waypoints'].append(get_post_checkout_waypoint(checkoutPoint))
    shopper['waypoints'].append(get_exit_point())
    shopper['waypoints'].append(get_origin_point())


def get_shopper_path(itemCount):
    locations = get_item_locations(itemCount)
    alpha = get_origin_point()
    if locations[0][1] >= alpha[1]:
        entry = get_entry_point()
    else:
        entry = get_exit_point()

    locations = reorder_locations(entry, locations)
    return [alpha] + [entry] + locations


def shoppers_enter_store(time):
    global shopperId
    #duration = int(random.normalvariate(40*60, 20*60))
    #duration = 5*60 if duration < 5*60 else duration
    items = int(random.normalvariate(25, 10))
    items = 1 if items < 1 else items
    #TODO: Make duration a factor of the path rather than item count
    duration = items * 60

    shopperPath = get_shopper_path(items)
    inThruExit = shopperPath[0][1] > shopperPath[1][1]

    chance = random.randint(1, 1 / CUSTOMERS_ENTERING_PER_SECOND)

    if chance == 1 / CUSTOMERS_ENTERING_PER_SECOND:
        shoppers.append(dict(id=shopperId, startTime=time, shoppingDuration=duration, itemCount=items, waypoints=shopperPath, reverseEntry=inThruExit))
        shopperId += 1


def shoppers_enter_queue(time):
    for shopper in shoppers:
        enterQueueTime = shopper['startTime'] + shopper['shoppingDuration']
        if enterQueueTime <= time:
            add_to_queue(shopper, time)
            shoppers.remove(shopper)


def nudge_the_queue(shoppers):
    for shopper in shoppers[1:]:
        waitPoint = list(shopper['waypoints'][-4])
        waitPoint[0] += 2
        shopper['waypoints'].insert(-4, waitPoint)


def complete_checkout(time, shoppersInQueue):
    shopperAtRegister = shoppersInQueue[0]
    checkoutDuration = shopperAtRegister['itemCount'] / ITEMS_SCANNED_PER_SECOND + TRANSACTION_TIME_SECONDS
    finishCheckoutTime = shopperAtRegister['startCheckoutTime'] + checkoutDuration
    if finishCheckoutTime <= time:
        shopperAtRegister['finishCheckoutTime'] = time
        satisfiedCustomers.append(shopperAtRegister)
        shoppersInQueue.pop(0)
        start_next_checkout(shoppersInQueue, time)
        #nudge_the_queue(shoppersInQueue)


def shoppers_checkout(time):
    for queue in queues:
        shoppersInQueue = queue['shoppers']
        if shoppersInQueue:
            complete_checkout(time, shoppersInQueue)


def run_store_simulation():
    time = 0

    initialize_queues()

    for time in range (0, SIM_DURATION):
        shoppers_enter_store(time)
        shoppers_enter_queue(time)
        shoppers_checkout(time)


def print_helpful_info():
    longestWait = 0;
    waitSum = 0;

    for customer in satisfiedCustomers:
        waitTime = customer['startCheckoutTime'] - customer['enterQueueTime']
        waitSum += waitTime
        if waitTime > longestWait:
            longestWait = waitTime
        #print customer['startTime'], customer['itemCount'], customer['enterQueueTime'], customer['startCheckoutTime'], customer['finishCheckoutTime'], waitTime / 60
    print "longest wait time =", longestWait / 60, "min"
    print "average wait time =", waitSum / len(satisfiedCustomers) / 60, "min"
    print "total customers =", len(satisfiedCustomers)
    print "shoppers left in store =", len(shoppers)
    print "total shoppers in sim =", len(satisfiedCustomers) + len(shoppers)


def load_points(filename, color=WHITE, cachedFile='', verbose=False):
    #TODO: Force reload
    if cachedFile == '':
        cachedFile = filename + '.cache'
    if os.path.isfile(cachedFile):
        with open(cachedFile, 'r') as cache:
            if verbose:
                print "loading", filename, "points from cache"
            return pickle.load(cache)

    if not os.path.isfile(filename):
        print "File not found:", filename
        return []
    
    pointsMap = misc.imread(filename)

    points = []
    for y in xrange(0,pointsMap.shape[0]):
        for x in xrange(pointsMap.shape[1]):
            if (pointsMap[y,x] == color).all():
                points.append([y,x]) 
        if verbose:
            update_progress(float(y)/(y_px-1))
    with open(cachedFile, 'w') as cache:
        pickle.dump(points, cache)
    return points

def load_data(verbose):
    global poi
    global entryPoints
    global exitPoints
    global originPoints
    global queueWaitPoints
    global checkoutPoints
    poi = load_points('waypoints_map.png', verbose)
    entryPoints = load_points('entry_points.png', verbose)
    exitPoints = load_points('exit_points.png', verbose)
    originPoints = load_points('origin_points.png', verbose)
    queueWaitPoints = load_points('queue_points.png', RED, 'queue_wait_points.cache', verbose)
    checkoutPoints = load_points('queue_points.png', GREEN, 'checkout_points.cache', verbose)

def generate_shopper_paths(shopperList, verbose):
    if verbose:
        print 'generating', len(shopperList), 'shopper paths'
    costMap = misc.imread('cost_map.png')
    mainMap = misc.imread('map.png')
    coarseMap = misc.imread('coarse_map.png')
    pathPool = Pool(processes=cpu_count())
    for shopper in shopperList:
        myPath = shopper['waypoints']
        shopper['path'] = pathPool.apply_async(pathfinder.find_path, [myPath, mainMap, costMap, coarseMap, verbose])
    pathPool.close()
    pathPool.join()
    for shopper in shopperList:
        shopper['path'] = shopper['path'].get()

def time_shopper_paths(shopperList, verbose):
    for shopper in shopperList:
        path = shopper['path']
        totalPoints = 0
        shoppingPoints = 0
        for segment in path:
            totalPoints += len(segment)

        sliceAt = -4 if 'assignedQueue' in shopper else -1
        for segment in path[:sliceAt]:
            shoppingPoints += len(segment)
    
        shopper['timeStep'] = float(shopper['shoppingDuration']) / shoppingPoints


def main():
    random.seed(0)
    opts = docopt(__doc__)
    verbose = False

    if opts.get('-v'):
        verbose = True

    load_data(verbose)

    run_store_simulation()

    generate_shopper_paths(shoppers + satisfiedCustomers, verbose)
    time_shopper_paths(shoppers + satisfiedCustomers, verbose)

    if verbose:
        print_helpful_info()

    simData = dict(UTC_REFERENCE=UTC_REFERENCE, CASHIER_COUNT=CASHIER_COUNT, queues=queues, satisfiedCustomers=satisfiedCustomers, shoppers=shoppers)

    if verbose:
        print 'dumping data to sim_dump.pickle'
    with open('sim_dump.pickle', 'w') as cache:
        pickle.dump(simData, cache)

if __name__ == '__main__':
    main()

