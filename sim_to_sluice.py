"""queue obs data protein generator

Usage: sim_to_sluice.py [-r input_file] [-f] [-v]

Options:
  -r  # read input from file (defaults to sim_dump.pickle)
  -f  # write proteins to file (defaults to pool)
  -v  # display simulation statistics
"""

from docopt import docopt
import itertools
import os.path
import pickle
import copy

from utilities import *
import sluice
from sluice.types import v3float64, float64
from retail import store_coords

def v3fromxy(x,y):
    lat, lon = store_coords.from_pixel_xy(x, y, from_cropped=False)
    return v3float64(lat, lon, 0.0)

def initialize_tripwire_locations():
    yield {
            'id': 'MainExit',
            'kind': 'entrance',
            'loc': v3fromxy (876, 813),
            'timestamp': UTC_REFERENCE,
            'attrs': {
                'type': 'left',
                'traffic_flow': '0',
                'direction': 'Exit'
                }
            }
    yield {
            'id': 'MainEntrance',
            'kind': 'entrance',
            'loc' : v3fromxy(1128, 813),
            'timestamp': UTC_REFERENCE,
            'attrs': {
                'type': 'right',
                'traffic_flow': '0',
                'direction': 'Enter'
                }
            }
    return

def initialize_queue_metrics_data():
    for i in range(0, CASHIER_COUNT):
        if queues[i]['laneOpen']:
            yield {
                    'id': 'queue%02d' % (i + 1),
                    'kind': 'queue_length',
                    'timestamp': UTC_REFERENCE,
                    'attrs': {
                        'people_in_line': str(0)
                        }
                    }
            yield {
                    'id': 'wait%02d' % (i + 1),
                    'kind': 'wait_time',
                    'timestamp': UTC_REFERENCE,
                    'attrs': {
                        'wait(sec)': str(0)
                        }
                    }
            continue
    return

def extract_queue_metrics_data():
    for customer in satisfiedCustomers:
        yield {
                'id': 'queue%02d' % customer['assignedQueue'],
                'kind': 'queue_length',
                'timestamp': customer['enterQueueTime'] + UTC_REFERENCE,
                'attrs': {
                    'people_in_line': str(customer['queueLength'])
                    }
                }
        yield {
                'id': 'wait%02d' % customer['assignedQueue'],
                'kind': 'wait_time',
                'timestamp': customer['enterQueueTime'] + UTC_REFERENCE,
                'attrs': {
                    'wait(sec)': str(customer['startCheckoutTime'] - customer['enterQueueTime'])
                    }
                }
        continue
    return


def extract_tripwire_data():
    DELAY = 30
    for customer in satisfiedCustomers + shoppers:
        enterTime = UTC_REFERENCE + customer['startTime'] + (len(customer['path'][0]) * customer['timeStep'])
        reverseEntry = customer['waypoints'][0][1] > customer['waypoints'][1][1]
        entrance = 'MainEntrance' if not reverseEntry else 'MainExit'
        #entrance = 'MainEntrance' if not customer['reverseEntry'] else 'MainExit'
        yield {
                'id': entrance,
                'kind': 'entrance',
                'timestamp': enterTime,
                'attrs': {
                    'traffic_flow': '1',
                    'direction': 'Enter'
                    }
                }
        yield {
                'id': entrance,
                'kind': 'entrance',
                'timestamp': enterTime + DELAY,
                'attrs': {
                    'traffic_flow': '0',
                    'direction': 'Enter'
                    }
                }
        if 'assignedQueue' in customer:
            exitTime = UTC_REFERENCE + customer['finishCheckoutTime'] + ((len(customer['path'][-3]) + len(customer['path'][-2])) * customer['timeStep'])
            yield {
                    'id': 'MainExit',
                    'kind': 'entrance',
                    'timestamp': exitTime,
                    'attrs': {
                        'traffic_flow': '1',
                        'direction': 'Exit'
                        }
                    }
            yield {
                    'id': 'MainExit',
                    'kind': 'entrance',
                    'timestamp': exitTime + DELAY,
                    'attrs': {
                        'traffic_flow': '0',
                        'direction': 'Exit'
                        }
                    }
        continue
    return


def extract_shopper_paths():
    customerList = satisfiedCustomers + shoppers
    for cIdx, customer in enumerate(customerList):
        stepCount = 0
        checkout = '0'
        timeReference = UTC_REFERENCE + customer['startTime'] 
        latLonPath = []
        for segIdx, segment in enumerate(customer['path']):
            if 'assignedQueue' in customer:
                if segIdx == (len(customer['path']) - 4):
                    checkout = '1'
                    #TODO: this isn't quite right
                    timeReference = UTC_REFERENCE + customer['startCheckoutTime']
                    stepCount = 0
                elif checkout == '1':
                    checkout = '0'
                    timeReference = UTC_REFERENCE + customer['finishCheckoutTime']
                    stepCount = 0
            for pIdx, point in enumerate(segment):
                #update_progress(float(pIdx)/(len(segment)-1))
                coord = v3fromxy(point[1],point[0])
                latLonPath.append(coord)
                if len(latLonPath) > 100:
                        latLonPath.pop(0)
                time = timeReference + (stepCount * customer['timeStep'])
                stepCount += 1
                yield {
                        'kind': 'shopper path',
                        'id': 'customer_path' + str(customer['id']),
                        'timestamp': time,
                        'path': list(latLonPath), #copy quickly -- not deeply
                        'attrs': {
                            'employee': '0',
                            }
                        }
                yield {
                        'kind': 'shopper cart',
                        'id': 'customer_cart' + str(customer['id']),
                        'timestamp': time,
                        'loc': coord,
                        'attrs': {
                            'active': '1',
                            'checkout': '0',#testing -- checkout,
                            'employee': '0',
                            }
                        }
            if len(customerList) == 1:
                update_progress(float(segIdx)/(len(customer['path'])-1))
        if 'assignedQueue' in customer:
            yield {
                    'kind': 'shopper cart',
                    'id': 'customer_cart' + str(customer['id']),
                    'timestamp': time+1,
                    'loc': coord,
                    'attrs': {
                        'active': '0',
                        'checkout': '0',
                        'employee': '0',
                        }
                    }
            yield {
                    'kind': 'shopper path',
                    'id': 'customer_path' + str(customer['id']),
                    'timestamp': time+1,
                    'path': "",
                    'attrs': {
                        'active': '0',
                        'employee': '0',
                        }
                    }
        if len(customerList) > 1:
            update_progress(float(cIdx+1)/len(customerList))
        continue
    return

def get_queue_metrics_data():
    return itertools.chain(initialize_queue_metrics_data(), extract_queue_metrics_data())


UTC_REFERENCE = 0
CASHIER_COUNT = 0
queues = []
shoppers = []
satisfiedCustomers = []
verbose = True

def main():

    global UTC_REFERENCE
    global CASHIER_COUNT
    global queues
    global shoppers
    global satisfiedCustomers
    global verbose

    opts = docopt(__doc__)
    output = None
    writeFiles = False
    filenames = ['queue_obs.protein','tripwire_locations.protein','tripwire_obs.protein','shopper_paths.protein']
    simFile = 'sim_dump.pickle'

    if opts.get('-r'):
        simFile = opts['-r']
    if opts.get('-v'):
        verbose = True
    if opts.get('-f'):
        writeFiles = True

    if os.path.isfile(simFile):
        with open(simFile, 'r') as simOutput:
            if verbose:
                print "loading", simFile
            simData = pickle.load(simOutput)
    else:
        print "input file", simFile, "not found, exiting..."
        exit()

    UTC_REFERENCE = simData['UTC_REFERENCE']
    queues = simData['queues']
    shoppers = simData['shoppers']
    satisfiedCustomers = simData['satisfiedCustomers']
    CASHIER_COUNT = simData['CASHIER_COUNT']

    if writeFiles:
        output = open(filenames[0], 'w')
        print 'writing', output.name
    s = sluice.api.Sluice(output=output)
    s.update_observations(get_queue_metrics_data())

    if writeFiles:
        output = open(filenames[1], 'w')
        print 'writing', output.name
    s = sluice.api.Sluice(output=output)
    s.inject_topology(initialize_tripwire_locations())

    if writeFiles:
        output = open(filenames[2], 'w')
        print 'writing', output.name
    s = sluice.api.Sluice(output=output)
    s.update_observations(extract_tripwire_data())

    if writeFiles:
        output = open(filenames[3], 'w')
        print 'writing', output.name
    s = sluice.api.Sluice(output=output)
    s.inject_topology(extract_shopper_paths())

    if writeFiles:
        print '\ndata files written, see:'
        for filename in filenames:
            print "  ", filename

if __name__ == '__main__':
    main()
