
def flood_fill(image, node, targetColor, replacementColor):
    from collections import deque
    if targetColor.tolist() == replacementColor:
        return

    q = deque([node])
    
    while len(q) > 0: 
        node = q.popleft()
        if (image[tuple(node)] == targetColor).all():
            image[tuple(node)] = replacementColor
            w = list(node)
            w[1] -= 1
            while (image[tuple(w)] == targetColor).all():
                image[tuple(w)] = replacementColor
                n = list(w)
                n[0] += 1
                if (image[tuple(n)] == targetColor).all():
                    q.append(n)
                s = list(w)
                s[0] -= 1
                if (image[tuple(s)] == targetColor).all():
                    q.append(s)
                w[1] -= 1
            e = list(node)
            e[1] += 1
            while (image[tuple(e)] == targetColor).all():
                image[tuple(e)] = replacementColor
                n = list(e)
                n[0] += 1
                if (image[tuple(n)] == targetColor).all():
                    q.append(n)
                s = list(e)
                s[0] -= 1
                if (image[tuple(s)] == targetColor).all():
                    q.append(s)
                e[1] += 1
        

def get_spiral(start, radius):
	points = []
	CELL_COUNT = (radius*2)**2 - 1
	unit_y, unit_x = start
	x_min = unit_x;
	x_max = unit_x;
	y_min = unit_y;
	y_max = unit_y;
	direction = 0;
	points.append((unit_y, unit_x))
	for i in range(0,CELL_COUNT):
		if direction == 0: #right
			unit_x += 1
			if unit_x > x_max:
				x_max = unit_x
				direction += 1
		elif direction == 1: #up
			unit_y -= 1
			if unit_y < y_min:
				y_min = unit_y
				direction += 1
		elif direction == 2: #left
			unit_x -= 1
			if unit_x < x_min:
				x_min = unit_x
				direction += 1
		elif direction == 3: #down
			unit_y += 1
			if unit_y > y_max:
				y_max = unit_y
				direction = 0
		else:
			direction = 0
			break
		points.append((unit_y, unit_x))
		
	return points
	

def update_progress(progress):
    import sys
    barLength = 10 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()	
