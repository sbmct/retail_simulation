"""
 
Generate maps for finding paths. This script generates the following files:
  
  "map.png" -- the boundary map that designates passible/impassible areas
  "obstacle_map.png" -- a negative of map.png
  "coarse_map.png" -- the boundary map that designates passible/impassible areas
  "coarse_obstacle_map.png" -- a negative of map.png
  "cost_map.png" -- the cost map for navigating the map
  "waypoints_map.png" -- points of interest on the map 

Usage: generate_maps.py [-f map.png] [-v] [-p color] [-m maps]

Options:
  -f PNG             # a png file of the model to be mapped 
  -v                 # display progress and maps
  -p r,g,b[,a]       # the rgba color for passible areas; default (184,184,184,255)
  -m [b][,r][,c][,w] # maps to generate, by default all maps are generated 
        b: boundary
        r: rough (coarse)
        c: cost
        w: waypoints
 
"""

from docopt import docopt
from scipy import misc
from scipy import ndimage
import numpy
import time
from PIL import Image
from utilities import *
import os.path
import random

def generate_main_maps():
    if opts.get('-v'):
        print "\nGenerating map..."
    for y in range(0, y_px):
        for x in range(0, x_px):
            if (user_map[y,x] == floor_color).all():
                obstacles[y,x] = False
                main_map[y,x] = True
        if opts.get('-v'):
            update_progress(float(y)/(y_px-1))
    print "saving map.png"
    misc.imsave('map.png', main_map)
    print "saving obstacle_map.png"
    misc.imsave('obstacle_map.png', obstacles)
    if opts.get('-v'):
        Image.open('map.png').show()
        Image.open('obstacle_map.png').show()


def generate_coarse_maps():
    scalar = 0.10;
    coarse_y = main_map.shape[0] * scalar + 1
    coarse_x = main_map.shape[1] * scalar + 1
    coarse_map = numpy.ones((coarse_y, coarse_x), numpy.bool)
    coarse_obstacle_map = numpy.zeros((coarse_y, coarse_x), numpy.bool)

    for y in range(0, main_map.shape[0]):
        cy = int(y * scalar)
        for x in range(0, main_map.shape[1]):
            cx = int(x * scalar)
            if main_map[y,x].all() == False:
                coarse_map[cy,cx] = False
                coarse_obstacle_map[cy,cx] = True
                x += 1 // scalar + 1 
        if opts.get('-v'):
            update_progress(float(y)/(main_map.shape[0]-1))

    print "saving coarse_map.png"
    misc.imsave('coarse_map.png', coarse_map)
    print "saving coarse_obstacle_map.png"
    misc.imsave('coarse_obstacle_map.png', coarse_obstacle_map)
    if opts.get('-v'):
        Image.open('coarse_boundary.png').show()
        Image.open('coarse_obstacle_map.png').show()

RADIUS = 5
CELL_COUNT = (RADIUS*2)**2
WHITE = 255
COLOR_FACTOR = float(CELL_COUNT) / WHITE
def calculate_color(y,x):
    unit_x = x;
    unit_y = y;
    
    x_min = unit_x;
    x_max = unit_x;
    y_min = unit_y;
    y_max = unit_y;
    
    spiral = get_spiral([unit_y,unit_x], RADIUS)
    
    sumWhite = 0
    sumBlack = 0
    for point in spiral:
        if main_map[point[0],point[1]].all() == True:
            sumWhite += 1
    
    color = float(sumWhite) / COLOR_FACTOR
    
    return color 

def generate_cost_map():
    cost_map = numpy.zeros(user_map.shape, numpy.int)
    if opts.get('-v'):
        print "\nGenerating cost map..."
    for y in range(0, y_px):
        for x in range(0, x_px):
            if (user_map[y,x] == floor_color).all():
                cost_map[y,x] = calculate_color(y,x)
        if opts.get('-v'):
            update_progress(float(y)/(y_px-1))
    print "saving cost_map.png"
    misc.imsave('cost_map.png', cost_map)
    if opts.get('-v'):
        Image.open('cost_map.png').show()


def is_edge(theMap, y, x, targetColor, excluded):
    edge = False
    if ((theMap[y,x] == targetColor).all()):
        if (not (theMap[y-1,x] == targetColor).all() or
            not (theMap[y+1,x] == targetColor).all() or
            not (theMap[y,x-1] == targetColor).all() or
            not (theMap[y,x+1] == targetColor).all()):
            edge = True
            for color in excluded:
                if ((theMap[y-1,x] == color).all() or
                    (theMap[y+1,x] == color).all() or
                    (theMap[y,x-1] == color).all() or
                    (theMap[y,x+1] == color).all()):
                        edge = False
                        break
    return edge

def generate_edge_map():
    if opts.get('-v'):
        print "\nGenerating edge map..."
        update_progress(0)
    passibleMap = user_map.copy() 
    targetColor = [255,0,255,255]
    flood_fill(passibleMap, (950,1000), floor_color, targetColor)
    edgeMap = numpy.zeros((y_px,x_px), numpy.bool)
    excludedEdges = [[0xc5,0xd6,0xe0,0xff],
                     [0x7a,0x7a,0x7a,0xff],
                     [0x00,0x00,0x00,0xff],
                     [0xff,0xff,0xff,0xff]]
    for y in range(0, y_px):
        for x in range(0, x_px):
            if is_edge(passibleMap, y, x, targetColor, excludedEdges):
                edgeMap[y,x] = True 
        if opts.get('-v'):
            update_progress(float(y)/(y_px-1))

    return edgeMap


def generate_waypoints_map():
    edgeMap = generate_edge_map()
    waypoints_map = numpy.zeros((y_px,x_px), numpy.bool)
    if opts.get('-v'):
        print "\nGenerating waypoints map..."
    for y in range(0, y_px):
        for x in range(0, x_px):
            if (edgeMap[y,x] == True):
                #TODO: This could be done in an orderly way
                if random.randint(0,5) == 0:
                    waypoints_map[y,x] = True 
        if opts.get('-v'):
            update_progress(float(y)/(y_px-1))
    print "saving waypoints_map.png"
    misc.imsave('waypoints_map.png', waypoints_map)
    if opts.get('-v'):
        Image.open('waypoints_map.png').show()



opts = docopt(__doc__)
if opts.get('-f'):
    filename = opts['-f']
else:
    filename = 'retail_store_with_fencing.png'

if not os.path.isfile(filename):
    print "File not found:", filename
    exit()

if opts.get('-p'):
    passible = [] 
    for part in opts['-p'].split(','):
        passible.append(int(part))
    if len(passible) == 3:
        passible.append(255)
    if opts.get('-v'):
        print "passible color", passible 
else:
    passible = (184, 184, 184, 255)
floor_color = numpy.array(passible)

user_map = misc.imread(filename)

y_px = user_map.shape[0]
x_px = user_map.shape[1]

obstacles = numpy.ones((y_px,x_px), numpy.bool)
main_map = numpy.zeros((y_px,x_px), numpy.bool)

genMain = genCoarse = genCost = genWaypoints = True

if opts.get('-m'):
    genMain = genCoarse = genCost = genWaypoints = False
    maps = opts['-m'].split(',')
    for map in maps:
        if map[0].lower() == 'b':
            genMain = True
        if map[0].lower() == 'r':
            genCoarse = True
        if map[0].lower() == 'c':
            genCost = True
        if map[0].lower() == 'w':
            genWaypoints = True

if genMain:
    generate_main_maps()
else:
    main_map = misc.imread('map.png')
if genCoarse:
    generate_coarse_maps()
if genCost:
    generate_cost_map()
if genWaypoints:
    generate_waypoints_map()
